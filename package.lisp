;;; -*- Mode: LISP; Syntax: COMMON-LISP; Base: 10 -*-
;;; arch-tag: 69AB34D5-8516-DBE0-76B43C49CD70

;;; Copyright (c) 2006, Kilian Sprotte. All rights reserved.

;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions
;;; are met:

;;;   * Redistributions of source code must retain the above copyright
;;;     notice, this list of conditions and the following disclaimer.

;;;   * Redistributions in binary form must reproduce the above
;;;     copyright notice, this list of conditions and the following
;;;     disclaimer in the documentation and/or other materials
;;;     provided with the distribution.

;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR 'AS IS' AND ANY EXPRESSED
;;; OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
;;; DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
;;; GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;;; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
;;; WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

(in-package :cl-user)

(defpackage :gecol
  (:documentation "Provides bindings to GECODE.")
  (:use :cl :iterate :cffi)
  (:export
   #:make-gecolspace
   #:make-dfs-space-int-int-stop
   #:make-bab-space-int-int-stop
   ;; Ints
   #:gec-minus
   ;; Sets
   #:gec-fs-make
   #:gec-fs-make-const
   #:gec-fs-make-bounds
   #:gec-fs-make-lower-bound
   #:gec-fs-make-upper-bound
   #:gec-fs-glb-size
   #:gec-fs-lub-size
   #:gec-fs-unknown-size
   #:gec-fs-card-min
   #:gec-fs-card-max
   #:gec-fs-lub-min
   #:gec-fs-lub-max
   #:gec-fs-glb-min
   #:gec-fs-glb-max
   #:gec-fs-contains
   #:gec-fs-not-contains
   #:gec-fs-assigned
   #:gec-fs-variable
;;; Sets - lisp extensions
   #:gec-fs-enumerate-lower-bound
   #:gec-fs-enumerate-upper-bound
   #:gec-fs-value
;;; utils
   #:with-list-as-int-array
   ))
